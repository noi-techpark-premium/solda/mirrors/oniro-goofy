# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://0001-fix-upstream-issue-352-heap-buffer-overflow-by-correcting-uint32t-underflow.patch"

